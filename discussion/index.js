// Function able to recieve data w/o the use of global variables of prompt();
function printName(name){
	console.log('My name is ' + name);
}
printName("Josh");
printName('Jimin');

function prinMyAge (age){
	console.log('I am ' + age);

}

prinMyAge(25);
prinMyAge();


function checkDivisivilityBy8(num){

	let remainder = num % 8;
	console.log('The remainder of ' + num + ' divided by 8 is: ' + remainder);

	let isDivisibleBy8 = remainder === 0;
	console.log('Is ' + num + ' divisible by 8?');
	console.log(isDivisibleBy8);
}

checkDivisivilityBy8(64);
checkDivisivilityBy8(27);


function printName(favSuperhero){
	console.log('My Favorite superhero is: ' + favSuperhero);
}
printName("Darna");

function printMyFavoriteLanguage (language){
	console.log("My Favorite language is: " + language);
}

printMyFavoriteLanguage('Javascript');
printMyFavoriteLanguage('Java');

function printFullName ( firstName,middleName,lastName){
	console.log(firstName + " " + middleName + " " + lastName);
}
printFullName("Josue","Diestro","Julian");

// Use Variables as Argumennts

let fName = "Larry";
let mName = "Joe";
let lName = "Bird";

printFullName(fName,mName,lName);


function printTop5FavSong (fav1, fav2, fav3, fav4, fav5){
	console.log("My Top 5 Favorite Songs are: " + fav1 + " " + fav2 + " " + fav3 + " " + fav4+ " and " + fav5);
	return fav1 +" " +fav5;
}

let sample1 = printTop5FavSong("adadas","sfsfsdf","sfsdfsdf","sfsdfsd","fsfdsdf");
console.log (sample1);

// let fullName = printFullName ("Mark", "Joseph", "Lacdao");
// console.log(fullName);


function returnFullname(firstName,middleName,lastName){

	return firstName + " " + middleName + " " + lastName;

}
fullName = returnFullname("Ernesto", "Antonio", "Maceda");
console.log(fullName);

console.log(fullName + " is my grandpa.")


function returnPhilippineAddress(city){

	return city + " , Philipppines"
}

let myFullAddress = returnPhilippineAddress("Cainta");
console.log(myFullAddress);


function checkDivisivilityBy4(num){

	let remainder = num % 4;

	let isDivisibleBy4 = remainder === 0;

	return isDivisibleBy4;
	console.log ("I am run after the return");
}

let num4isDivisibleBy4 = checkDivisivilityBy4(4);
let num14isDivisibleBy4 = checkDivisivilityBy4(14);


console.log(num4isDivisibleBy4);
console.log(num14isDivisibleBy4);


function createPlayerInfo (username, level , job){

	return "username: " + username + ", level: " +
		level + ", job: " +job;


}

let user1 = createPlayerInfo("white_night",95, "Paladin");
console.log(user1);